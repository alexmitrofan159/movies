package com.company.movies.model.remote

import com.company.movies.model.remote.entities.MoviesResponse
import io.reactivex.Single
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class MoviesApiRepository : MoviesApiSource {

    private val apiService: MoviesApiService by lazy {
        val client = OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().apply {
                setLevel(HttpLoggingInterceptor.Level.NONE)
            }).build()
        Retrofit.Builder().baseUrl(BASE_URL).client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build().create(MoviesApiService::class.java)
    }

    override fun fetchPopularMovies(page: Int): Single<MoviesResponse> {
        return apiService.getPopularMovies(createOptions(page)).prepareImages()
    }

    override fun fetchTopRatedMovies(page: Int): Single<MoviesResponse> {
        return apiService.getTopRatedMovies(createOptions(page)).prepareImages()
    }

    private fun Single<MoviesResponse>.prepareImages(): Single<MoviesResponse> {
        return map { movieResponse ->
            movieResponse.results.map { it.posterPath = IMAGE_START_URL + it.posterPath }
            movieResponse
        }
    }

    private fun createOptions(page: Int) = hashMapOf(
        "api_key" to "dad8a59d86a2793dda93aa485f7339c1",
        "page" to page.toString()
    )
}