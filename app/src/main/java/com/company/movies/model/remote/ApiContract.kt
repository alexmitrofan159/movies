package com.company.movies.model.remote

const val BASE_URL = "https://api.themoviedb.org/"

const val IMAGE_START_URL = "https://image.tmdb.org/t/p/w500"