package com.company.movies.model.remote

import com.company.movies.model.remote.entities.MoviesResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface MoviesApiService {

    @GET("/3/movie/popular")
    fun getPopularMovies(@QueryMap options: Map<String, String>): Single<MoviesResponse>

    @GET("/3/movie/top_rated")
    fun getTopRatedMovies(@QueryMap options: Map<String, String>): Single<MoviesResponse>
}