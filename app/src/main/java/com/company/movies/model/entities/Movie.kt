package com.company.movies.model.entities

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Movie(
    @SerializedName("id") val id: Int = 0,
    @SerializedName("title") val title: String = "",
    @SerializedName("poster_path") var posterPath: String = "",
    @SerializedName("overview") val overview: String = "",
    @SerializedName("release_date") val releaseDate: String = "",
    @SerializedName("original_title") val originalTitle: String = "",
    @SerializedName("original_language") val originalLanguage: String = "",
    @SerializedName("popularity") val popularity: Float = 0f,
    @SerializedName("vote_count") val voteCount: Int = 0,
    @SerializedName("vote_average") val voteAverage: Float = 0f,
    @SerializedName("video") val isVideo: Boolean = false
) : Parcelable