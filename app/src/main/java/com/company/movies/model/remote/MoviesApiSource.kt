package com.company.movies.model.remote

import com.company.movies.model.remote.entities.MoviesResponse
import io.reactivex.Single

interface MoviesApiSource {

    fun fetchPopularMovies(page: Int): Single<MoviesResponse>

    fun fetchTopRatedMovies(page: Int): Single<MoviesResponse>
}