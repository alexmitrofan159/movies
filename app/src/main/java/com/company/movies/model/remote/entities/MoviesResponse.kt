package com.company.movies.model.remote.entities

import com.company.movies.model.entities.Movie
import com.google.gson.annotations.SerializedName

class MoviesResponse(
    @SerializedName("page") val page: Int = 0,
    @SerializedName("results") val results: List<Movie> = listOf(),
    @SerializedName("total_results") val totalResults: Int = 0,
    @SerializedName("total_pages") val totalPages: Int = 0
)