package com.company.movies

import android.app.Application
import com.company.movies.injection.remoteModule
import com.company.movies.injection.schedulersModule
import com.company.movies.injection.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class BaseApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@BaseApplication)
            modules(listOf(remoteModule, schedulersModule, viewModelModule))
        }
    }
}