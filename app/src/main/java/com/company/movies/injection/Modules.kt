package com.company.movies.injection

import com.company.movies.model.remote.MoviesApiRepository
import com.company.movies.model.remote.MoviesApiSource
import com.company.movies.ui.main.MoviesViewModel
import com.company.movies.util.schedulers.BaseSchedulerProvider
import com.company.movies.util.schedulers.SchedulerProvider
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * These are all the modules which will be injected with Koin
 */

val remoteModule = module {
    single { MoviesApiRepository() as MoviesApiSource }
}

val schedulersModule = module {
    single { SchedulerProvider() as BaseSchedulerProvider }
}

val viewModelModule = module {
    viewModel { MoviesViewModel(get(), get(), get()) }
}