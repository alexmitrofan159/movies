package com.company.movies.util.schedulers

import io.reactivex.Scheduler

/**
 * Allow providing different types of Providers
 */

interface BaseSchedulerProvider {

    fun computation(): Scheduler

    fun io(): Scheduler

    fun ui(): Scheduler
}