package com.company.movies.ui.base

import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

class EndScrollDetector(
    private val layoutManager: GridLayoutManager,
    private val onEnd: () -> Unit
) :
    RecyclerView.OnScrollListener() {

    private var prevTotal = 0
    private var loading = true
    private val visibleThreshold = 5

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        val visibleItemCount = recyclerView.childCount
        val totalItemCount = layoutManager.itemCount
        val firstVisibleItem = layoutManager.findFirstVisibleItemPosition()

        if (loading) {
            if (totalItemCount > prevTotal) {
                loading = false
                prevTotal = totalItemCount
            }
        }
        if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
            if (totalItemCount > 1)
                onEnd()
            loading = true
        }
    }

    fun reload() {
        loading = true
        prevTotal = 0
    }
}