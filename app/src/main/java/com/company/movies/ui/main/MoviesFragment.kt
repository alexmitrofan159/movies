package com.company.movies.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.company.movies.R
import com.company.movies.databinding.FragmentMoviesBinding
import com.company.movies.ui.base.EndScrollDetector
import com.company.movies.ui.details.MovieDetailsActivity
import com.company.movies.ui.main.adapter.MoviesAdapter
import com.google.android.material.snackbar.Snackbar
import org.koin.android.ext.android.inject

class MoviesFragment : Fragment() {

    private lateinit var binding: FragmentMoviesBinding
    private val viewModel: MoviesViewModel by inject()

    private var endScrollDetector: EndScrollDetector? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMoviesBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
        val ordinal = arguments?.getInt(MOVIES_TYPE_EXTRAS)
        ordinal?.let { viewModel.start(MoviesType.values()[it]) }
    }

    override fun onResume() {
        super.onResume()
        observeEvents()
    }

    private fun setupViews() {
        val adapter = MoviesAdapter { MovieDetailsActivity.start(requireContext(), it) }
        val spanCount = resources.getInteger(R.integer.span_count)
        val layoutManager = GridLayoutManager(requireContext(), spanCount)
        layoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return if (adapter.getItemViewType(position) == MoviesAdapter.TYPE_FOOTER) spanCount else 1
            }
        }
        endScrollDetector = EndScrollDetector(layoutManager) { viewModel.loadMore() }
        binding.recyclerMovies.addOnScrollListener(endScrollDetector!!)
        binding.recyclerMovies.layoutManager = layoutManager
        binding.recyclerMovies.adapter = adapter
        binding.refreshLayout.setOnRefreshListener { viewModel.refresh() }
    }

    private fun observeEvents() {
        viewModel.snackBarLiveEvent.observe(viewLifecycleOwner, Observer {
            Snackbar.make(binding.recyclerMovies, it, Snackbar.LENGTH_SHORT).show()
        })
        viewModel.hideRefreshEvent.observe(viewLifecycleOwner, Observer {
            binding.refreshLayout.isRefreshing = false
        })
        viewModel.clearScrollDetectorEvent.observe(viewLifecycleOwner, Observer {
            endScrollDetector?.reload()
        })
    }

    enum class MoviesType {
        POPULAR, TOP_RATED
    }

    companion object {
        fun newInstance(moviesType: MoviesType) = MoviesFragment().apply {
            arguments = Bundle().also {
                it.putInt(MOVIES_TYPE_EXTRAS, moviesType.ordinal)
            }
        }

        private const val MOVIES_TYPE_EXTRAS = "movies_type"
    }
}
