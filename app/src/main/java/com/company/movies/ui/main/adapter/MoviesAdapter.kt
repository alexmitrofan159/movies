package com.company.movies.ui.main.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.company.movies.databinding.ItemFooterBinding
import com.company.movies.databinding.ItemMovieBinding
import com.company.movies.model.entities.Movie

class MoviesAdapter(private val onClick: (Movie) -> Unit) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var items = listOf<Movie>()
    private var isFooterVisible = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            TYPE_MOVIE -> MovieViewHolder(ItemMovieBinding.inflate(inflater, parent, false))
            else -> FooterViewHolder(ItemFooterBinding.inflate(inflater, parent, false))
        }
    }

    override fun getItemCount(): Int {
        return items.size + 1
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder.itemViewType) {
            TYPE_FOOTER -> (holder as? FooterViewHolder)?.bind(isFooterVisible)
            TYPE_MOVIE -> (holder as? MovieViewHolder)?.bind(items[position], onClick)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == items.size) TYPE_FOOTER else TYPE_MOVIE
    }

    fun setupData(items: List<Movie>, footerVisible: Boolean) {
        this.items = items
        this.isFooterVisible = footerVisible
        notifyDataSetChanged()
    }

    companion object {
        const val TYPE_FOOTER = 0
        private const val TYPE_MOVIE = 1
    }
}