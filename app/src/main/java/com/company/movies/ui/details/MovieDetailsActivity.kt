package com.company.movies.ui.details

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.company.movies.R
import com.company.movies.databinding.ActivityMovieDetailsBinding
import com.company.movies.model.entities.Movie

class MovieDetailsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMovieDetailsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_movie_details)
        val movie = intent.getParcelableExtra<Movie>(MOVIE_EXTRAS)
        binding.movie = movie
    }

    companion object {
        fun start(context: Context, movie: Movie) {
            context.startActivity(Intent(context, MovieDetailsActivity::class.java).apply {
                putExtra(MOVIE_EXTRAS, movie)
            })
        }

        private const val MOVIE_EXTRAS = "movie"
    }
}
