package com.company.movies.ui.main.adapter

import androidx.recyclerview.widget.RecyclerView
import com.company.movies.databinding.ItemFooterBinding

class FooterViewHolder(private val binding: ItemFooterBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(isVisible: Boolean) {
        binding.apply {
            this.visible = isVisible
            executePendingBindings()
        }
    }
}