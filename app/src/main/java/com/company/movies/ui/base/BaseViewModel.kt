package com.company.movies.ui.base

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.company.movies.util.schedulers.BaseSchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseViewModel(application: Application, schedulerProvider: BaseSchedulerProvider) :
    AndroidViewModel(application) {

    //Here it's stored all disposables which emmit RX observables. All the disposables should be disposed when
    //the viewModel is destroyed for avoiding memory leaks or useless calls.
    private val cd = CompositeDisposable()

    //These are the schedulers which helps to specify the needed thread for work
    protected val io = schedulerProvider.io()
    protected val ui = schedulerProvider.ui()

    protected fun runDisposable(disposable: () -> Disposable) {
        cd.add(disposable())
    }

    protected fun getString(resId: Int): String {
        return getApplication<Application>().getString(resId)
    }

    protected fun clearDisposables() {
        cd.clear()
    }

    override fun onCleared() {
        super.onCleared()
        cd.dispose()
    }
}