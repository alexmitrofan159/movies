package com.company.movies.ui.main

import android.app.Application
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.LiveData
import com.company.movies.R
import com.company.movies.model.entities.Movie
import com.company.movies.model.remote.MoviesApiSource
import com.company.movies.model.remote.entities.MoviesResponse
import com.company.movies.ui.base.BaseViewModel
import com.company.movies.util.SingleLiveEvent
import com.company.movies.util.schedulers.BaseSchedulerProvider
import io.reactivex.Single
import retrofit2.HttpException
import java.io.IOException

class MoviesViewModel(
    private val apiSource: MoviesApiSource,
    application: Application,
    schedulerProvider: BaseSchedulerProvider
) : BaseViewModel(application, schedulerProvider) {

    val progressVisible = ObservableBoolean(false)
    val movies = ObservableArrayList<Movie>()
    val hasMore = ObservableBoolean(false)
    val noMoviesVisible = ObservableBoolean(false)

    //It's good practice to post all values in LiveData from viewModel but not from View Layer
    //The view will be able to observe only the LiveData events
    private val _snackBarLiveEvent = SingleLiveEvent<String>()
    val snackBarLiveEvent: LiveData<String>
        get() = _snackBarLiveEvent
    private val _hideRefreshEvent = SingleLiveEvent<Unit>()
    val hideRefreshEvent: LiveData<Unit>
        get() = _hideRefreshEvent
    private val _clearScrollDetectorEvent = SingleLiveEvent<Unit>()
    val clearScrollDetectorEvent: LiveData<Unit>
        get() = _clearScrollDetectorEvent

    private var moviesType: MoviesFragment.MoviesType? = null
    private var page = 1

    private var dataLoading = false
    private var dataLoaded = false

    fun start(moviesType: MoviesFragment.MoviesType) {
        this.moviesType = moviesType
        if (!dataLoading && !dataLoaded) {
            if (movies.isEmpty()) progressVisible.set(true)
            loadMovies(true)
        }
    }

    private fun loadMovies(clearOldData: Boolean) {
        dataLoading = true
        dataLoaded = false
        when (moviesType) {
            MoviesFragment.MoviesType.POPULAR -> fetchMovies(clearOldData) {
                apiSource.fetchPopularMovies(page)
            }
            MoviesFragment.MoviesType.TOP_RATED -> fetchMovies(clearOldData) {
                apiSource.fetchTopRatedMovies(page)
            }
        }
    }

    private fun fetchMovies(clearOldData: Boolean, function: () -> Single<MoviesResponse>) {
        runDisposable {
            function()
                .subscribeOn(io)
                .observeOn(ui)
                .subscribe({
                    if (clearOldData) movies.clear()
                    hasMore.set(it.page < it.totalPages)
                    movies.addAll(it.results)
                    if (movies.isEmpty() && it.results.isEmpty()) noMoviesVisible.set(true)
                    progressVisible.set(false)
                    dataLoading = false
                    dataLoaded = true
                    _hideRefreshEvent.postValue(null)
                }, {
                    progressVisible.set(false)
                    dataLoading = false
                    _hideRefreshEvent.postValue(null)
                    when (it) {
                        is IOException -> _snackBarLiveEvent.postValue(getString(R.string.error_network))
                        is HttpException -> _snackBarLiveEvent.postValue(
                            getApplication<Application>().getString(
                                R.string.error_http,
                                it.code(),
                                it.localizedMessage
                            )
                        )
                        else -> _snackBarLiveEvent.postValue(it.localizedMessage)
                    }
                })
        }
    }

    fun loadMore() {
        if (hasMore.get()) {
            page++
            loadMovies(false)
        }
    }

    fun refresh() {
        _clearScrollDetectorEvent.call()
        clearDisposables()
        page = 1
        dataLoading = false
        loadMovies(true)
    }
}