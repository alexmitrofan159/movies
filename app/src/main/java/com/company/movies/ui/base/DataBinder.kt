package com.company.movies.ui.base

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.company.movies.model.entities.Movie
import com.company.movies.ui.main.adapter.MoviesAdapter

@BindingAdapter("app:imageUrl")
fun ImageView.setImageUrl(url: String?) {
    Glide.with(context).load(url).transition(DrawableTransitionOptions.withCrossFade()).into(this)
}

@BindingAdapter("app:movies", "app:footerVisible", requireAll = true)
fun RecyclerView.setMovies(movies: List<Movie>, isFooterVisible: Boolean) {
    (adapter as? MoviesAdapter)?.setupData(movies, isFooterVisible)
}