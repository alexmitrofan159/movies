package com.company.movies.ui.main.adapter

import androidx.recyclerview.widget.RecyclerView
import com.company.movies.databinding.ItemMovieBinding
import com.company.movies.model.entities.Movie

class MovieViewHolder(private val binding: ItemMovieBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(movie: Movie, onClick: (Movie) -> Unit) {
        binding.apply {
            this.movie = movie
            cardMovie.setOnClickListener { onClick(movie) }
            executePendingBindings()
        }
    }
}